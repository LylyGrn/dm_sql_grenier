--Exercice 2: Mise en place des tables :

CREATE database bibliotheque;
USE bibliotheque ;

--Creation des tables

CREATE TABLE ouvrage(
	isbn BIGINT(10) NOT NULL,
	titre VARCHAR(200) NOT NULL,
	auteur VARCHAR(80),
	genre CHAR(5) NOT NULL,
	editeur VARCHAR(80),
	CONSTRAINT pk_ouvrage PRIMARY KEY(isbn)
)engine=InnoDB;

CREATE TABLE exemplaire(
	numero SMALLINT(3) NOT NULL AUTO_INCREMENT,
	isbn BIGINT(10) NOT NULL,
	etat CHAR(5) NOT NULL,
	CONSTRAINT pk_exemplaire PRIMARY KEY(numero, isbn)
)engine=InnoDB;

CREATE TABLE genre(
	code VARCHAR(5) NOT NULL,
	libelle VARCHAR(80) NOT NULL,
	CONSTRAINT pk_genre PRIMARY KEY(code)
)engine=InnoDB;

CREATE TABLE membre(
	numero INTEGER(10) NOT NULL AUTO_INCREMENT,
	nom VARCHAR(80) NOT NULL,
	prenom VARCHAR(80) NOT NULL,
	adresse VARCHAR(200) NOT NULL,
	telephone VARCHAR(10) NOT NULL,
	adhesion DATETIME NOT NULL,
	duree TINYINT(2) NOT NULL,
	CONSTRAINT pk_membre PRIMARY KEY(numero)
)engine=InnoDB;

CREATE TABLE emprunt(
	numero INTEGER(10) NOT NULL AUTO_INCREMENT,
	membre INTEGER(10) NOT NULL,
	creeLe DATETIME NOT NULL,
	CONSTRAINT pk_emprunt PRIMARY KEY(numero)
)engine=InnoDB;

CREATE TABLE detailsemprunt(
	emprunt INTEGER(10) NOT NULL,
	numero SMALLINT(3) NOT NULL,
	isbn BIGINT(10) NOT NULL,
	exemplaire SMALLINT(3) NOT NULL,
	renduLe DATETIME NOT NULL,
	CONSTRAINT pk_detailsemprunt PRIMARY KEY(emprunt, numero)
)engine=InnoDB;


--Ajout des contraintes

ALTER TABLE ouvrage ADD CONSTRAINT fk_ouvrage_genre
	FOREIGN KEY(genre) REFERENCES genre (code) ON DELETE CASCADE ON UPDATE CASCADE;
	
ALTER TABLE exemplaire ADD CONSTRAINT fk_exemplaire_ouvrage
	FOREIGN KEY(isbn) REFERENCES ouvrage (isbn) ON DELETE CASCADE ON UPDATE CASCADE;
	
ALTER TABLE exemplaire ADD CONSTRAINT chk_exemplaire
	CHECK (etat='NE' OR etat='BO' OR etat='MO' OR etat='MA');
	
ALTER TABLE membre ADD CONSTRAINT chk_exemplaire_duree
	CHECK (duree>0);
	
ALTER TABLE emprunt ADD CONSTRAINT fk_emprunt_membre
	FOREIGN KEY(membre) REFERENCES membre (numero) ON DELETE CASCADE ON UPDATE CASCADE;
	
	
ALTER TABLE detailsemprunt ADD CONSTRAINT fk_detailsemprunt_emprunt
	FOREIGN KEY(emprunt) REFERENCES emprunt (numero) ON DELETE CASCADE ON UPDATE CASCADE;
	
ALTER TABLE detailsemprunt ADD CONSTRAINT fk_detailsemprunt_exemplaire
	FOREIGN KEY(isbn) REFERENCES exemplaire (isbn) ON DELETE CASCADE ON UPDATE CASCADE;
	
ALTER TABLE detailsemprunt ADD CONSTRAINT fk_detailsemprunt_exemplaire_1
	FOREIGN KEY(exemplaire) REFERENCES exemplaire (numero) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE membre 
	ADD UNIQUE (telephone);
	
ALTER TABLE detailsemprunt 
	MODIFY COLUMN renduLe DATETIME DEFAULT NULL; 
	
ALTER TABLE emprunt	
	MODIFY COLUMN creeLe DATETIME DEFAULT NOW();
	
--Exercice 3: Création de séquences :

ALTER TABLE membre AUTO_INCREMENT = 10;

--Exercice 4: Ajout de contraintes d’intégrité :

ALTER TABLE membre 
	ADD UNIQUE (nom);
	
ALTER TABLE membre 
	ADD UNIQUE (prenom);
	
--Exercice 5: Modification de table et ajout d’une colonne :

ALTER TABLE membre 
	CHANGE telephone telephonefixe VARCHAR(10);
	
ALTER TABLE membre
	ADD telephoneport VARCHAR(10) NOT NULL;
	
ALTER TABLE membre 
	ADD UNIQUE (telephoneport);
	
ALTER TABLE membre ADD CONSTRAINT chk_membre
	CHECK (telephoneport LIKE '06%' OR '07%');
	
--Exercice 6: Création d’un index: 
	
CREATE INDEX idx_genre
	ON ouvrage (genre);
	
CREATE INDEX idx_membre
	ON emprunt (membre);
	
CREATE INDEX idx_isbn
	ON detailsemprunt (isbn);
	
CREATE INDEX idx_exemplaire
	ON detailsemprunt (exemplaire);
	
CREATE INDEX idx_isbn
	ON exemplaire (isbn);
	
CREATE INDEX idx_emprunt
	ON detailsemprunt (emprunt);

--Exercice 7: Modification d’une contrainte d’intégrité :

ALTER TABLE detailsemprunt 
	DROP FOREIGN KEY fk_detailsemprunt_emprunt;

ALTER TABLE detailsemprunt ADD CONSTRAINT fk_detailsemprunt_emprunt
	FOREIGN KEY(emprunt) REFERENCES emprunt (numero) ON DELETE CASCADE ON UPDATE CASCADE;
	
--Exercice 8: Attribution d’une valeur par défaut à une colonne :

ALTER TABLE exemplaire
	ALTER etat SET DEFAULT 'NE';
	
--Exercice 9: Définition d’un synonyme :

CREATE VIEW abonnes AS 
	SELECT membre.numero, membre.nom, membre.prenom, membre.adresse, 
	membre.adhesion, membre.duree, membre.telephonefixe, membre.telephoneport FROM membre;
	
--Exercice 10: Modification du nom d’une table : 

RENAME TABLE detailsemprunt TO details; 

--PARTIE 2 : Langage de manipulation des données
--Exercice 1: 

--insertion des valeurs
--table genre 

INSERT INTO genre VALUES ('REC', 'Récit');
INSERT INTO genre VALUES ('POL', 'Policier');
INSERT INTO genre VALUES ('BD', 'Bande Dessinée');
INSERT INTO genre VALUES ('INF', 'Informatique');
INSERT INTO genre VALUES ('THE', 'Théâtre');
INSERT INTO genre VALUES ('ROM', 'Roman');

--table ouvrage

INSERT INTO ouvrage VALUES('2203314168','LEFRANC-L\'ultimatum', 'Martin, Carin', 'BD', 'Casterman');
INSERT INTO ouvrage VALUES('2746021285', 'HTML entrainez-vous pour maitriser le code', 'Luc Van Lancker', 'INF', 'ENI');
INSERT INTO ouvrage VALUES('2746026090', 'SQL', 'J. Gabillaud', 'INF', 'ENI');
INSERT INTO ouvrage VALUES('2266085816', 'Pantagruel', 'François Rabelais', 'ROM', 'POCKET');
INSERT INTO ouvrage VALUES('2266091611', 'Voyage au centre de la terre', 'Jules Verne', 'ROM', 'POCKET');
INSERT INTO ouvrage VALUES('2253010219', 'Le crime de l’Orient Express', 'Agatha Christie', 'POL', 'Livre Poche');
INSERT INTO ouvrage VALUES('2070400816', 'Le Bourgeois gentilhomme', 'Molière', 'THE', 'Gallimard');
INSERT INTO ouvrage VALUES('2070367177', 'Le curé de Tours', 'Honoré de Balzac', 'ROM', 'Gallimard');
INSERT INTO ouvrage VALUES('2080720872', 'Boule de suif', 'Guy de Maupassant', 'REC', 'Flammarion');
INSERT INTO ouvrage VALUES('2877065073', 'La gloire de mon père', 'Marcel Pagnol', 'ROM', 'Fallois');
INSERT INTO ouvrage VALUES('2020549522', 'L’aventure des manuscrits de la mer morte', NULL, 'REC', 'Seuil');
INSERT INTO ouvrage VALUES('2253006327', 'Vingt mille lieues sous les mers', 'Jules Verne', 'ROM', 'LGF');
INSERT INTO ouvrage VALUES('2038704015', 'De la terre à la lune', 'Jules Verne', 'ROM', 'Larousse');

--table exemplaire

INSERT INTO exemplaire VALUES(1, '2203314168', 'Moyen');
INSERT INTO exemplaire VALUES(2, '2203314168', 'Bon');
INSERT INTO exemplaire VALUES(3, '2203314168', 'Neuf');
INSERT INTO exemplaire VALUES(1, '2746021285', 'Bon');
INSERT INTO exemplaire VALUES(1, '2746026090', 'Bon');
INSERT INTO exemplaire VALUES(2, '2746026090', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2266085816', 'Bon');
INSERT INTO exemplaire VALUES(2, '2266085816', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2266091611', 'Bon');
INSERT INTO exemplaire VALUES(2, '2266091611', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2253010219', 'Bon');
INSERT INTO exemplaire VALUES(2, '2253010219', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2070400816', 'Bon');
INSERT INTO exemplaire VALUES(2, '2070400816', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2070367177', 'Bon');
INSERT INTO exemplaire VALUES(2, '2070367177', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2080720872', 'Bon');
INSERT INTO exemplaire VALUES(2, '2080720872', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2877065073', 'Bon');
INSERT INTO exemplaire VALUES(2, '2877065073', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2020549522', 'Bon');
INSERT INTO exemplaire VALUES(2, '2020549522', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2253006327', 'Bon');
INSERT INTO exemplaire VALUES(2, '2253006327', 'Moyen');
INSERT INTO exemplaire VALUES(1, '2038704015', 'Bon');
INSERT INTO exemplaire VALUES(2, '2038704015', 'Moyen');

--Exercice 2: Utilisation d’une séquence: 

--insertion valeurs table membre

INSERT INTO membre VALUES(1, 'ALBERT', 'Anne', '13 rue des Alpes', NULL, DATE_SUB(Sysdate(), INTERVAL 60 DAY), 1, '0601020304');
INSERT INTO membre VALUES(2, 'BERNAUD', 'Barnabé', '6 rue des bécasses', NULL, DATE_SUB(Sysdate(), INTERVAL 10 DAY), 3, '0602030105');
INSERT INTO membre VALUES(3, 'CUVARD', 'Camille', '52 rue des cerisiers', NULL, DATE_SUB(Sysdate(), INTERVAL 100 DAY), 6, '0602010509');
INSERT INTO membre VALUES(4, 'DUPOND', 'Daniel', '11 rue des daims', NULL, DATE_SUB(Sysdate(), INTERVAL 250 DAY), 12, '0610236515');
INSERT INTO membre VALUES(5, 'EVROUX', 'Eglantine', '34 rue des elfes', NULL, DATE_SUB(Sysdate(), INTERVAL 150 DAY), 6, '0658963125');
INSERT INTO membre VALUES(6, 'FREGEON', 'Fernand', '11 rue des Frans', NULL, DATE_SUB(Sysdate(), INTERVAL 400 DAY), 6, '0602036987');
INSERT INTO membre VALUES(7, 'GORIT', 'Gaston', '96 rue de la glacerie', NULL, DATE_SUB(Sysdate(), INTERVAL 150 DAY), 1, '0684235781');
INSERT INTO membre VALUES(8, 'HEVARD', 'Hector', '12 reu haute', NULL, DATE_SUB(Sysdate(), INTERVAL 250 DAY), 12, '0608546578');
INSERT INTO membre VALUES(9, 'INGRAND', 'Irène', '54 rue de iris', NULL, DATE_SUB(Sysdate(), INTERVAL 50 DAY), 12, '0605020409');
INSERT INTO membre VALUES(10, 'JUSTE', 'Julien', '5 place des Jacobins', NULL, DATE_SUB(Sysdate(), INTERVAL 100 DAY), 6, '0603069876');

--Exercice 3: Exécution d’un script :

--insertion valeurs table emprunt

insert into emprunt(numero, membre, creele) values(1,1,DATE_SUB(sysdate(),INTERVAL 200 DAY));
insert into emprunt(numero, membre, creele) values(2,3,DATE_SUB(sysdate(),INTERVAL 190 DAY));
insert into emprunt(numero, membre, creele) values(3,4,DATE_SUB(sysdate(),INTERVAL 180 DAY));
insert into emprunt(numero, membre, creele) values(4,1,DATE_SUB(sysdate(),INTERVAL 170 DAY));
insert into emprunt(numero, membre, creele) values(5,5,DATE_SUB(sysdate(),INTERVAL 160 DAY));
insert into emprunt(numero, membre, creele) values(6,2,DATE_SUB(sysdate(),INTERVAL 150 DAY));
insert into emprunt(numero, membre, creele) values(7,4,DATE_SUB(sysdate(),INTERVAL 140 DAY));
insert into emprunt(numero, membre, creele) values(8,1,DATE_SUB(sysdate(),INTERVAL 130 DAY));
insert into emprunt(numero, membre, creele) values(9,9,DATE_SUB(sysdate(),INTERVAL 120 DAY));
insert into emprunt(numero, membre, creele) values(10,6,DATE_SUB(sysdate(),INTERVAL 110 DAY));
insert into emprunt(numero, membre, creele) values(11,1,DATE_SUB(sysdate(),INTERVAL 100 DAY));
insert into emprunt(numero, membre, creele) values(12,6,DATE_SUB(sysdate(),INTERVAL 90 DAY));
insert into emprunt(numero, membre, creele) values(13,2,DATE_SUB(sysdate(),INTERVAL 80 DAY));
insert into emprunt(numero, membre, creele) values(14,4,DATE_SUB(sysdate(),INTERVAL 70 DAY));
insert into emprunt(numero, membre, creele) values(15,1,DATE_SUB(sysdate(),INTERVAL 60 DAY));
insert into emprunt(numero, membre, creele) values(16,3,DATE_SUB(sysdate(),INTERVAL 50 DAY));
insert into emprunt(numero, membre, creele) values(17,1,DATE_SUB(sysdate(),INTERVAL 40 DAY));
insert into emprunt(numero, membre, creele) values(18,5,DATE_SUB(sysdate(),INTERVAL 30 DAY));
insert into emprunt(numero, membre, creele) values(19,4,DATE_SUB(sysdate(),INTERVAL 20 DAY));
insert into emprunt(numero, membre, creele) values(20,1,DATE_SUB(sysdate(),INTERVAL 10 DAY));

--insertion valeurs table details

insert into details(emprunt, numero, isbn, exemplaire, rendule) values(1,1,2038704015,1,DATE_SUB(sysdate(),INTERVAL 195 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(1,2,2070367177,2,DATE_SUB(sysdate(),INTERVAL 190 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(2,1,2080720872,1,DATE_SUB(sysdate(),INTERVAL 180 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(2,2,2203314168,1,DATE_SUB(sysdate(),INTERVAL 179 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(3,1,2038704015,1,DATE_SUB(sysdate(),INTERVAL 170 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(4,1,2203314168,2,DATE_SUB(sysdate(),INTERVAL 155 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(4,2,2080720872,1,DATE_SUB(sysdate(),INTERVAL 155 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(4,3,2266085816,1,DATE_SUB(sysdate(),INTERVAL 159 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(5,1,2038704015,1,DATE_SUB(sysdate(),INTERVAL 140 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(6,1,2266085816,2,DATE_SUB(sysdate(),INTERVAL 141 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(6,2,2080720872,2,DATE_SUB(sysdate(),INTERVAL 130 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(6,3,2746021285,1,DATE_SUB(sysdate(),INTERVAL 133 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(7,1,2070367177,2,DATE_SUB(sysdate(),INTERVAL 100 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(8,1,2080720872,1,DATE_SUB(sysdate(),INTERVAL 116 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(9,1,2038704015,1,DATE_SUB(sysdate(),INTERVAL 100 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(10,1,2080720872,2,DATE_SUB(sysdate(),INTERVAL 107 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(10,2,2746026090,1,DATE_SUB(sysdate(),INTERVAL 78 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(11,1,2746021285,1,DATE_SUB(sysdate(),INTERVAL 81 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(12,1,2203314168,1,DATE_SUB(sysdate(),INTERVAL 86 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(12,2,2038704015,1,DATE_SUB(sysdate(),INTERVAL 60 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(13,1,2070367177,1,DATE_SUB(sysdate(),INTERVAL 65 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(14,1,2266091611,1,DATE_SUB(sysdate(),INTERVAL 66 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(15,1,2070400816,1,DATE_SUB(sysdate(),INTERVAL 50 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(16,1,2253010219,2,DATE_SUB(sysdate(),INTERVAL 41 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(16,2,2070367177,2,DATE_SUB(sysdate(),INTERVAL 41 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(18,1,2070367177,1,DATE_SUB(sysdate(),INTERVAL 14 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(19,1,2746026090,1,DATE_SUB(sysdate(),INTERVAL 12 DAY));
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(20,1,2266091611,1,default);
insert into details(emprunt, numero, isbn, exemplaire, rendule) values(20,2,2253010219,1,default);

--insertion valeur table ouvrage

insert into ouvrage (isbn, titre, auteur, genre, editeur) values (2080703234, 'Cinq semaines en ballon', 'Jules Verne', 'ROM', 'Flammarion');

--Exercice 4: Extraction simple d’informations :

SELECT * FROM ouvrage;
SELECT * FROM exemplaire;
SELECT * FROM genre;
SELECT * FROM membre;
SELECT * FROM emprunt;
SELECT * FROM details;

--Exercice 5: Ajout d’une colonne :

ALTER TABLE details 
	ADD etat VARCHAR(2) DEFAULT 'EC'; 
	
ALTER TABLE details ADD CONSTRAINT chk_details
	CHECK (etat='EC' OR etat='RE');
	
UPDATE details 
	SET etat='RE' WHERE emprunt!= 20;
	
--Exercice 6: Mise à jour conditionnelle :



--Exercice 7: Suppression de lignes :



--Exercice 8: Extraction du catalogue général des livres :

SELECT * FROM ouvrage;

--Exercice 9: Extraction du tableau de bord des emprunts :

SELECT m.numero, m.nom, m.prenom, m.adresse, m.adhesion, m.duree, m.telephoneport, o.titre
	FROM membre m, ouvrage o, exemplaire ex, details d, emprunt em 
		WHERE o.isbn = ex.isbn AND ex.isbn = d.isbn AND d.emprunt = em.numero AND m.numero = em.membre AND DATEDIFF(d.renduLe, em.creeLe) > 15 GROUP BY m.nom, o.titre;
		
SELECT m.numero, m.nom, m.prenom, m.adresse, m.adhesion, m.duree, m.telephoneport, o.titre
	FROM membre m, ouvrage o, exemplaire ex, details d, emprunt em 
		WHERE o.isbn = ex.isbn AND ex.isbn = d.isbn AND d.emprunt = em.numero AND m.numero = em.membre AND DATEDIFF(d.renduLe, em.creeLe) < 15 GROUP BY m.nom, o.titre;
		
--Exercice 10: Extraction du nombre d’ouvrages dans chaque catégorie :

SELECT o.genre, COUNT(o.isbn) FROM ouvrage o, exemplaire ex WHERE o.isbn=ex.isbn GROUP BY o.genre;
		
--Exercice 11: Calcul de la durée moyenne d’un emprunt :

   --moyenne par membre		
SELECT AVG(DATEDIFF(rendule, creele)) AS 'durée moyenne', em.membre FROM details d, emprunt em WHERE d.emprunt=em.numero GROUP BY em.membre; 

   --moyenne de tous les membres
SELECT AVG(DATEDIFF(rendule, creele)) AS 'durée moyenne' FROM details d, emprunt em WHERE d.emprunt=em.numero; 

--Exercice 12: Durée moyenne de l’emprunt en fonction du genre de livre :
	
SELECT o.genre, AVG(DATEDIFF(rendule, creele)) AS 'durée moyenne' FROM emprunt em, details d, exemplaire ex, ouvrage o 
	WHERE em.numero=d.emprunt AND d.isbn=ex.isbn AND ex.isbn=o.isbn GROUP BY o.genre; 
	
--Exercice 13: Restriction sur un calcul d’agrégat :

SELECT ex.isbn FROM exemplaire ex, details d, emprunt em
	WHERE ex.isbn=d.isbn AND ex.numero=d.exemplaire AND d.emprunt=em.numero AND em.creele BETWEEN DATE_SUB(SYSDATE(), INTERVAL 1 YEAR) AND SYSDATE()
		GROUP BY ex.isbn HAVING COUNT(em.creele)>10;


--Exercice 14: Jointure externe :

SELECT o.isbn, o.titre, o.auteur, o.genre, o.editeur, ex.numero FROM ouvrage o LEFT JOIN exemplaire ex ON o.isbn=ex.isbn ORDER BY o.titre;

--Exercice 15: Création de vue et calcul du nombre d’emprunts pour chaque membre :

CREATE VIEW nombreemprunts AS
	SELECT m.nom, m.prenom, em.membre, COUNT(*) AS 'nombreemprunts' FROM details d, emprunt em, membre m 
		WHERE d.emprunt=em.numero AND em.membre=m.numero AND d.rendule IS NULL GROUP BY em.membre;
		
--Exercice 16: Création de vue et calcul du nombre d’emprunts par ouvrage :

CREATE VIEW nbemprunts AS
	SELECT d.isbn, COUNT(*) AS 'nombreemprunts' FROM details d, emprunt em, exemplaire ex
		WHERE em.numero=d.emprunt AND d.isbn=ex.isbn AND ex.numero=d.exemplaire 
			GROUP BY ex.isbn ORDER BY ex.isbn;

--Execice 17: Le tri: 

SELECT * FROM membre ORDER BY nom ASC;

--Exercice 18: Extraction complexe et mise en place d’un tableau de bord :

SELECT o.genre, COUNT(ex.numero) AS 'Mois 1', 0 as 'Mois 2', 0 as 'Mois 3' FROM details d,
exemplaire ex, ouvrage o, emprunt em
WHERE o.isbn=ex.isbn AND ex.isbn=d.isbn AND em.numero=d.emprunt AND d.rendule
BETWEEN DATE_SUB(SYSDATE(), INTERVAL 1 MONTH) AND DATE_SUB(SYSDATE(),
INTERVAL 0 MONTH) GROUP BY o.genre
UNION
SELECT  o.genre, 0 as 'Mois 1', COUNT(ex.numero) AS 'Mois 2', 0 as 'Mois 3' FROM details d,
exemplaire ex, ouvrage o, emprunt em
WHERE o.isbn=ex.isbn AND ex.isbn=d.isbn AND em.numero=d.emprunt AND d.rendule
BETWEEN DATE_SUB(SYSDATE(), INTERVAL 2 MONTH) AND DATE_SUB(SYSDATE(),
INTERVAL 1 MONTH) GROUP BY o.genre
UNION
SELECT  o.genre, 0 as 'Mois 1', 0 as 'Mois 2', COUNT(ex.numero) AS 'Mois 3' FROM details d,
exemplaire ex, ouvrage o, emprunt em
WHERE o.isbn=ex.isbn AND ex.isbn=d.isbn AND em.numero=d.emprunt AND d.rendule
BETWEEN DATE_SUB(SYSDATE(), INTERVAL 3 MONTH) AND DATE_SUB(SYSDATE(),
INTERVAL 2 MONTH) GROUP BY o.genre;

--Exercice 19: Etablir la liste des livres :

SELECT genre, group_concat(titre separator "  / ")as 'titre' FROM ouvrage GROUP BY genre; 

--Partie 3: SQL Avancé: 
--Exercice 1: Calcul d’agrégat :

SELECT o.titre, ex.numero, COUNT(em.creele) FROM ouvrage o, exemplaire ex, details d, emprunt em 
	WHERE o.isbn=ex.isbn AND ex.isbn=d.isbn AND d.emprunt=em.numero AND d.exemplaire=ex.numero 
		GROUP BY o.titre, ex.numero WITH ROLLUP;

--Exercice 2: Sous-requête corrélée :

SELECT DISTINCT o.titre, ex.numero FROM details d, exemplaire ex, ouvrage o 
	WHERE o.isbn=ex.isbn AND ex.isbn=d.isbn AND d.exemplaire=ex.numero AND d.rendule>PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM SYSDATE()), 201902) 
		ORDER BY o.titre;

SELECT o.titre, ex.numero FROM details d, exemplaire ex, ouvrage o 
WHERE o.isbn=ex.isbn AND ex.isbn=d.isbn AND d.exemplaire=ex.numero AND rendule>DATE_SUB(SYSDATE(), INTERVAL 3 MONTH) ORDER BY o.titre;

--Exercice 3: Sous-requêtes imbriquées :

SELECT o.titre, ex.numero FROM ouvrage o, exemplaire ex WHERE o.isbn=ex.isbn AND ex.etat!='Neuf'; 

--Exercice 4: Recherche sur un critère exprimé sous forme de chaine de caractères :

SELECT * FROM ouvrage WHERE titre LIKE '%mer%';

--Exercice 5: Les expressions régulières :

SELECT auteur FROM ouvrage WHERE auteur LIKE '% de %';

--Exercice 6: Affichage détaillé des libellés : 

SELECT isbn, titre,  
	CASE 
		WHEN genre='BD' THEN 'Jeunesse'
		WHEN genre='INF' THEN 'Professionnel'
		WHEN genre='POL' THEN 'Adulte'
		ELSE 'Tous'
	END AS 'Public'
FROM ouvrage; 

--Exercice 7: Définition de commentaires : 

ALTER TABLE membre COMMENT = "Descriptif des membres. Possède le synonyme Abonnes";
ALTER TABLE genre COMMENT = "Définition des genres possibles des ouvrages";
ALTER TABLE ouvrage COMMENT = "Description des ouvrages référencés par la bibliothèque";
ALTER TABLE exemplaire COMMENT = "Définition précise des livres présents dans la bibliothèque";
ALTER TABLE emprunt COMMENT ="Fiche d’emprunt de livres, toujours associée à un et un seul membre";
ALTER TABLE details COMMENT ="Chaque ligne correspond à un livre emprunté";

--Exercice 8: Interrogations des commentaires :
SELECT table_name, table_comment FROM INFORMATION_SCHEMA.TABLES 
WHERE table_name='membre' OR table_name='genre' OR table_name='ouvrage' OR table_name='exemplaire' OR table_name='emprunt' OR table_name='details'; 

--Exercice 9: Suppression d’une table : 

DROP TABLE details;

--Exercice 10: Message affiché en fonction du résultat d’un calcul :

SELECT o.isbn, o.titre,
	CASE
		WHEN COUNT(ex.numero)=0 THEN 'Aucun'
		WHEN COUNT(ex.numero)<3 THEN 'Peu'
		WHEN COUNT(ex.numero)<6 THEN 'Normal'
		WHEN COUNT(ex.numero)>5 THEN 'Beaucoup'
	END AS 'Nombre Exemplaires'
FROM ouvrage o, exemplaire ex 
	WHERE o.isbn=ex.isbn GROUP BY o.titre;

--Exercice 11: Tableau récapitulatif :

SELECT t.isbn,
	SUM(IF(t.exemplaire=1,t.qte,NULL)) AS '1',
	SUM(IF(t.exemplaire=2,t.qte,NULL)) AS '2'
FROM (SELECT isbn, exemplaire, COUNT(*) AS qte FROM details GROUP BY isbn, exemplaire) t GROUP BY t.isbn;
	


--Exercice 12: Colonne virtuelle :

CREATE VIEW finadhesion AS SELECT *, DATE_ADD(adhesion, INTERVAL duree MONTH) AS 'Fin adhésion' FROM membre;